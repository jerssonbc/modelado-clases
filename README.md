# modelado-clases

Desarrollo de modelado de clases para Figuras Geométricas

## Consideraciones
- El diseño de diagrama de clases en UML se encuentra en el archivo [class diagram.png](https://gitlab.com/jerssonbc/modelado-clases/-/blob/main/class-diagram.png)
- Las clases de la solución se encuentra dentro de la ruta:
  ```
  src/main/java/com/jbc/figuras
  ```