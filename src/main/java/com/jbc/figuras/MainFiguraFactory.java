/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras;

/**
 *
 * @author jersson
 */
public class MainFiguraFactory {
    public static void main(String[] args) {
        FiguraGeometricaFactory  figuraGeometricaFactory = new FiguraGeometricaFactoryImpl();        
        Figura cuadrado = figuraGeometricaFactory.crearFigura(
                new RegistroFigura("Cuadrado",4,4,0));
        Figura triangulo = figuraGeometricaFactory.crearFigura(
                new RegistroFigura("Triangulo",4,3,0));        
        Figura circulo = figuraGeometricaFactory.crearFigura(
                new RegistroFigura("Circulo",0,0,0.5));
        
        System.out.println(cuadrado.mostrarDatos());
        System.out.println(triangulo.mostrarDatos());
        System.out.println(circulo.mostrarDatos());
    }
}
