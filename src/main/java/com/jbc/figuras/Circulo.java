/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras;

import lombok.Data;

/**
 *
 * @author jersson
 */
@Data
public class Circulo extends Figura implements Calculo{
    final double PI=3.1416;
    private double radio;    

    public Circulo() {
        super();
    }

    public Circulo(String tipoFigura, double radio) {
        super(tipoFigura);
        this.radio = radio;
    }   
    
    @Override
    public double obtenerSuperfice() {
        return PI*getRadio()*getRadio();
    }

    @Override
    public double obtenerBase() {
        return 0.0;
    }

    @Override
    public double obtenerAltura() {
        return 0.0;
    }

    @Override
    public double obtenerDiametro() {
        return 2*getRadio();
    }

    @Override
    public String obtenerTipoFigura() {
        return getTipoFigura();
    }

    @Override
    String mostrarDatos() {
        return "Tipo: "+getTipoFigura()+
                "\nSuperficie: "+obtenerSuperfice()+
                "\nBase: "+obtenerBase()+
                "\nAltura: "+obtenerAltura()+
                "\nDiametro: "+obtenerDiametro()+
                "\n";
    }
    
}
