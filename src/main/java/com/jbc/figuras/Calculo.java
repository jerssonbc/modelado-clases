/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras;

/**
 *
 * @author jersson
 */
public interface Calculo {
    double obtenerSuperfice();
    double obtenerBase();
    double obtenerAltura();
    double obtenerDiametro();
    String obtenerTipoFigura();
}
