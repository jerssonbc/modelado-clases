/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras;

/**
 *
 * @author jersson
 */
public class FiguraGeometricaFactoryImpl implements FiguraGeometricaFactory{

    @Override
    public Figura crearFigura(RegistroFigura registroFigura) {
        Figura figura = null;
        switch(registroFigura.getTipoFigura().toUpperCase()){
            case "CUADRADO":
                figura = new Cuadrado(registroFigura.getTipoFigura(), 
                                       registroFigura.getBase(), registroFigura.getAltura());
                break;
            case "TRIANGULO":
                figura = new Triangulo(registroFigura.getTipoFigura(), 
                                       registroFigura.getBase(), registroFigura.getAltura());
                break;
            case "CIRCULO":
                figura = new Circulo(registroFigura.getTipoFigura(), 
                                    registroFigura.getRadio());
                break;
        }
        return figura;
    }    
}
