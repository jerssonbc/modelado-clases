/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras;

import lombok.Data;

/**
 *
 * @author jersson
 */
@Data
public class RegistroFigura {
    private String tipoFigura;
    private double base;
    private double altura;
    private double radio;

    public RegistroFigura(String tipoFigura, double base, double altura, double radio) {
        this.tipoFigura = tipoFigura;
        this.base = base;
        this.altura = altura;
        this.radio = radio;
    }
    
}
