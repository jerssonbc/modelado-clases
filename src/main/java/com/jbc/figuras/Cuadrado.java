/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jbc.figuras;

import lombok.Data;

/**
 *
 * @author jersson
 */
@Data
public class Cuadrado extends Figura implements Calculo {
    private double base;
    private double altura;

    public Cuadrado() {
    }
        
    public Cuadrado(String tipoFigura, double base, double altura) {
        super(tipoFigura);
        this.base = base;
        this.altura = altura;
    }    
    
    @Override
    public double obtenerSuperfice() {
        return getBase()*getAltura();
    }

    @Override
    public double obtenerBase() {
        return getBase();
    }

    @Override
    public double obtenerAltura() {
        return getAltura();
    }

    @Override
    public double obtenerDiametro() {
        return 0.0;
    }

    @Override
    public String obtenerTipoFigura() {
        return getTipoFigura();
    }    

    @Override
    String mostrarDatos() {
       return "Tipo: "+getTipoFigura()+
                "\nSuperficie: "+obtenerSuperfice()+
                "\nBase: "+obtenerBase()+
                "\nAltura: "+obtenerAltura()+
                "\nDiametro: "+obtenerDiametro()+
               "\n";
    }
}
